//
//  ViewController.swift
//  ExpandMenu
//
//  Created by Savana on 15/08/2016.
//  Copyright © 2016 Savana. All rights reserved.
//

import UIKit
//http://economictimes.indiatimes.com/small-biz/startups/move-over-bengaluru-here-is-how-belagavi-udupi-and-mysuru-are-charting-their-own-startup-success/articleshow/53575710.cms?utm_source=twitter.com&utm_medium=referral&utm_campaign=ETTWMain
class ViewController: UIViewController {

    @IBOutlet weak var vw_widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var vw_menuHolder: UIView!
    @IBOutlet weak var btn_postOrCancel:UIButton!;
    @IBOutlet weak var btn_equalHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btn_equalWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var btn_fb:UIButton!
    @IBOutlet weak var btn_width: UIView!
    @IBOutlet weak var lc_width: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        vw_widthConstraint.constant = 60;
        self.lc_width.constant = 0;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func expandOrCompressMenu(_ sender: AnyObject) {
        let width = (vw_widthConstraint.constant == 60 ) ? 240 : 60;
        let expand = (width == 240) ? true : false;
        vw_widthConstraint.constant = CGFloat(width);
        let duration = expand ? 0.45 : 0.30
        let angle = expand ? -15 : 0
        for view in self.vw_menuHolder.subviews
        {
            if view.tag != -1
            {
                view.isHidden = !expand;
            }
        }        
        UIView.animate(withDuration: duration, animations: {
            self.view.layoutIfNeeded();
            self.btn_postOrCancel.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(angle)));
            }) { (completed) in
                if completed
                {
                    UIView.animate(withDuration: duration, animations: {
                        let final:CGFloat   = expand ? 25 : 0 ;
                        self.lc_width.constant = final;
                        self.view.layoutIfNeeded();
                    })
                    
                }
        }
    }

}

